const express = require('express');
const pathToSwaggerUi = require('swagger-ui-dist').absolutePath();
var path = require('path');
var request = require('request');
var session = require('express-session');
const jwt = require("jsonwebtoken");
const bodyParser = require('body-parser');
var sessionstorage = require('sessionstorage');

const app = express();
const SECRET_KEY = process.env.SECRET_KEY;
if (SECRET_KEY == undefined || SECRET_KEY == '')
    throw ('Please provide environment variable SECRET_KEY.');
const TENANT = process.env.TENANT_NAME;
if (TENANT == undefined || TENANT == '')
    throw ('Please provide environment variable TENANT_NAME.');
const IDENTITY_URL = process.env.IDENTITY_URL;
if (IDENTITY_URL == undefined || IDENTITY_URL == '')
    throw ('Please provide environment variable IDENTITY_URL.');
const CONFIGURATION_NAME = process.env.CONFIGURATION_NAME;
if (CONFIGURATION_NAME == undefined || CONFIGURATION_NAME == '')
    throw ('Please provide environment variable CONFIGURATION_NAME.');
const CONFIGURATION_URL = process.env.CONFIGURATION_URL;
if (CONFIGURATION_URL == undefined || CONFIGURATION_URL == '')
    throw ('Please provide environment variable CONFIGURATION_URL.');
    
app.use((req, res, next) => {
    if (!req.body) {
        req.body = {};
    }
    if (!req.local) {
        req.local = {};
    }
    next();
});

app.use(bodyParser.json());

app.get('/', function (req, res) {
    res.redirect('/login');
});

app.use(express.static(pathToSwaggerUi));

app.get('/login', function (req, res) {
    res.sendFile(path.join(__dirname + '/login-template.html'));
});
app.post('/loginin', function (req, res) {
    let options = {
        url: IDENTITY_URL + "/login",
        headers: {
            'Authorization': getToken(),
            'Content-Type': 'application/json'
        },
        body: req.body,
        json: true
    };
    request.post(options, function (error, response, body) {
        if (!error && response.statusCode == 200) {
            sessionstorage.setItem('token', response.body.token);
            res.sendFile(path.join(__dirname + '/index.html'), { token: response.body.token });
        } else {
            console.log('Error in login: ' + error + ' response: ' + response.statusCode + ' response status message: ' + response.statusMessage);
            throw ('Error in login: ' + error + ' response: ' + response.statusCode + ' response status message: ' + response.statusMessage);
        }
    });
});
app.get('/logout', function (req, res) {
    sessionstorage.clear();
    return res.redirect('/');
});
app.get('/urls', function (req, res) {
    let options = {
        url: CONFIGURATION_URL + "/" + CONFIGURATION_NAME,
        headers: {
            'Authorization': sessionstorage.getItem('token'),
            'Content-Type': 'application/json'
        },
        json: true
    };
    request.get(options, function (error, response, body) {
        if (!error && response.statusCode == 200) {
            res.json(response.body.swaggerurls);
        } else {
            console.log('Error in retrieving configuration: ' + CONFIGURATION_NAME + ' response: ' + response.statusCode + ' response status message: ' + response.statusMessage);
            throw ('Error in retrieving configuration: ' + CONFIGURATION_NAME + ' response: ' + response.statusCode + ' response status message: ' + response.statusMessage);
        }
    });
});
app.get('/index', function (req, res) {
    res.sendFile(path.join(__dirname + '/index.html'));
});
app.get('/token', function (req, res, next) {
    res.json({ token: sessionstorage.getItem('token') });
});

function getToken() {
    if (SECRET_KEY === undefined) { return null }
    var key = Buffer.from(SECRET_KEY, 'base64');
    var currentDate = new Date();
    expiration = addMinutes(currentDate, 1);
    token = jwt.sign({ "tenant": TENANT, "sub": TENANT + "-swagger-ui", "iss": "lendfoundry", "IsValid": true }, key, { expiresIn: 1 * 60 }); // expires in 24 hours
    return "Bearer " + token;
}

function addMinutes(date, m) {
    date.setTime(date.getTime() + (m * 60 * 1000));
    return date;
}

app.listen(3000);
