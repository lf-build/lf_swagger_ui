FROM node:9-slim

RUN npm install -g pm2

WORKDIR /app
ADD /package.json /app/package.json
RUN npm install --only=production

ADD . /app

ENTRYPOINT pm2-docker --raw start app.js